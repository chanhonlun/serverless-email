## Description
An email scheduler using [AWS Lambda](https://aws.amazon.com/lambda) and 
[AWS Step Function](https://aws.amazon.com/step-functions), based on 
[serverless](https://www.serverless.com) framework.

## Prerequisite
- Verify an email address in [AWS SES](https://aws.amazon.com/ses)
- Update `EMAIL_SENDER_EMAIL_ADDRESSES` in [`config.json`](./config.json)

## Deploy with command line
make sure you have login to aws and then run
```
npm run deploy:dev
```

## Usage
After deploying the application, you could schedule email by making a 
`POST` request to the api gateway endpoint, which looks like 
`https://<api-id>.execute-api.<aws-region>.amazonaws.com/<stage>`
```json
{
    "sendAt": "2021-06-10T08:00:00.000Z",
    "email": {
        "from": "default",
        "to": ["test@example.com"],
        "subject": "Hello World",
        "html": "Hello long time no see."
    }
}
```

## Request Parameters 
#### sendAt
(optional) The time that the email will be sent out. If pastime/ null, 
current time will be given.

#### email.from
(optional) Since this service support multiple sender, pass the key from the 
environment config. If null, it will be the first pair in config.

#### email.to
List of recipients' email

#### email.cc
(optional) List of CCs' email

#### email.bcc
(optional) List of BCCs' email

#### email.replyTo
(optional) Set reply-to in header, same values as `email.from`

#### email.subject
Email subject

#### email.html
Email body in HTML format

#### email.watchHtml
(optional) Email body in HTML for Apple Watch preview

## Response
#### id
ID for this request

## AWS CodePipeline setup guide
Since my source code will be pushed to public repository as well, I separated 
the environment config to another repository (privately inside AWS). Therefore, 
there will be 2 part in Source stage.

As you can see in [`buildspec.dev.yml`](./buildspec.dev.yml), I override the 
config from ConfigArtifact to my original config from SourceArtifact. Remember 
to set artifact name of deployment config to `ConfigArtifact` 

CodeBuild's execution role should also have the permission as stated in this 
[`policy.json`](./serverless-email-dev-ap-southeast-1-policy.json)

![CodePipeline sample](./readme-images/codepipeline-sample.png)

