import { get } from 'lodash';

import { config } from './config';

function getSenders(): { [key: string]: { title?: string; email: string } } {
  return config('email.email-addresses');
}

function isNoSenders(): boolean {
  return getKeys().length <= 0;
}

export function getKeys(): string[] {
  return Object.keys(getSenders());
}

function defaultKey(): string {
  if (isNoSenders()) throw new Error('senders not provided');

  return getKeys()[0];
}

function getDefaultSender(): string {
  const key = defaultKey();

  if (!key) return '';

  return getSender(key);
}

export function getSender(key?: string): string {
  if (!key) return getDefaultSender();

  const sender = get(getSenders(), key);

  if (!sender) return getDefaultSender();

  const { title, email } = sender;

  return title ? `${title} <${email}>` : email;
}
