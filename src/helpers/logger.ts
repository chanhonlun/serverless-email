import * as winston from 'winston';

import { config } from './config';

const winstonLogger = winston.createLogger({
  level: config('logger.level'),
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.splat(),
    winston.format.printf(({ timestamp, level, message }) => {
      return `${timestamp} ${level} - ${message}`;
    })
  ),
  transports: [new winston.transports.Console()],
});

function stringify(arg: string | object): string {
  return typeof arg === 'string' ? arg : JSON.stringify(arg);
}

function log(logFn, a1, a2) {
  if (!a2) {
    logFn(stringify(a1));
  } else {
    logFn(stringify(a1) + ' ' + stringify(a2));
  }
}

function debug(tag: string, message: string);
function debug(tag: string, object: any);
function debug(message: string);
function debug(object: any);
function debug(a1: string | object, a2?: string | object) {
  log(winstonLogger.debug, a1, a2);
}

function info(tag: string, message: string);
function info(tag: string, object: any);
function info(message: string);
function info(object: any);
function info(a1: string | object, a2?: string | object) {
  log(winstonLogger.info, a1, a2);
}

function warn(tag: string, message: string);
function warn(tag: string, object: any);
function warn(message: string);
function warn(object: any);
function warn(a1: string | object, a2?: string | object) {
  log(winstonLogger.warn, a1, a2);
}

function error(tag: string, message: string);
function error(tag: string, object: any);
function error(message: string);
function error(object: any);
function error(a1: string | object, a2?: string | object) {
  log(winstonLogger.error, a1, a2);
}

export const logger = { debug, info, warn, error };
