export const app = {
  name: process.env.APP_NAME,

  'state-machine': {
    arn: process.env.STATEMACHINE_ARN,
  },
};
