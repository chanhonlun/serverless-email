export const email = {
  'email-addresses': JSON.parse(
    process.env.EMAIL_SENDER_EMAIL_ADDRESSES ?? '{}'
  ),
};
