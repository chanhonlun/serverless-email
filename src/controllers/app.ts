import { lambda } from 'lambda-api-helper';

import { config, logger } from '@helpers';

export const getInfo = lambda.extend(async (request) => {
  logger.info('called getInfo');
  return { app: config('app.name') };
});
