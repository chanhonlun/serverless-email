import * as MailComposer from 'nodemailer/lib/mail-composer';

import { EmailOptions } from '../interfaces';

export async function getRawEmail(email: EmailOptions) {
  return new Promise((resolve, reject) => {
    const mail = new MailComposer(email).compile();
    mail.keepBcc = true;
    mail.build((error, message) => (error ? reject(error) : resolve(message)));
  });
}
