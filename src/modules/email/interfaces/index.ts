interface EmailSchedulingEventEmail {
  from?: string;

  to: string | string[];

  cc?: string | string[];

  bcc?: string | string[];

  replyTo?: string;

  subject: string;

  html: string;

  watchHtml?: string;
}

export interface EmailSchedulingEvent {
  id: string;
  sendAt: string; // ISO8601 date string
  email: EmailSchedulingEventEmail;
}

/**
 * {@link https://nodemailer.com/extras/mailcomposer/}
 */
export interface EmailOptions {
  /**
   * The e-mail address of the sender.
   * All e-mail addresses can be plain `sender@server.com` or
   * formatted `Sender Name <sender@server.com>`
   */
  from: string;

  /**
   * Comma separated list or an array of recipients e-mail addresses
   * that will appear on the To: field
   */
  to: string | string[];

  /**
   * Comma separated list or an array of recipients e-mail addresses
   * that will appear on the Cc: field
   */
  cc?: string | string[];

  /**
   * Comma separated list or an array of recipients e-mail addresses
   * that will appear on the Bcc: field
   */
  bcc?: string | string[];

  /**
   * An e-mail address that will appear on the Reply-To: field
   */
  replyTo?: string;

  /**
   * The subject of the e-mail
   */
  subject: string;

  /**
   * The HTML version of the message as an Unicode string.<br>
   * Provide either `text` or `html`
   */
  html: string;

  /**
   * Apple Watch specific HTML version of the message,
   * same usage as with `text` and `html`.
   * Latest watches have no problems rendering text/html content
   * so watchHtml is most probably never seen by the recipient
   */
  watchHtml?: string;
}
