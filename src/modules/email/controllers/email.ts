import { StepFunctions } from 'aws-sdk';
import { lambda } from 'lambda-api-helper';
import { nanoid } from 'nanoid';

import { config, logger, mail } from '@helpers';
import { validator } from '@validator';

export const schedule = lambda.extend(
  {
    middlewares: [
      validator.body({
        sendAt: 'date',
        email: 'object|required',
        'email.from': `string|in:${mail.getKeys().join(',')}`,
        'email.to': 'array|required|min:1',
        'email.to.*': 'string|email',
        'email.cc': 'array',
        'email.cc.*': 'string|email',
        'email.bcc': 'array',
        'email.bcc.*': 'string|email',
        'email.replyTo': `string|in:${mail.getKeys().join(',')}`,
        'email.subject': 'string|required',
        'email.html': 'string|required',
        'email.watchHtml': 'string',
      }),
      validator.handleErrors(),
    ],
  },
  async (request, response) => {
    const id = nanoid();
    const stateMachineArn = config('app.state-machine.arn');

    const result = await new StepFunctions()
      .startExecution({
        stateMachineArn,
        input: JSON.stringify({
          id,
          sendAt: request.body.sendAt ?? new Date().toISOString(),
          email: request.body.email,
        }),
      })
      .promise();

    logger.info(
      `State machine ${stateMachineArn} executed successfully`,
      result
    );

    response.status(201);
    return {
      id,
      // result,
    };
  }
);
