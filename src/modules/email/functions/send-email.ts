import { SES } from 'aws-sdk';

import { logger, mail } from '@helpers';

import { EmailService } from '../services';
import { EmailOptions, EmailSchedulingEvent } from '../interfaces';

export async function handle({ email }: EmailSchedulingEvent) {
  logger.info('send email, email', email);

  // @ts-ignore
  const options: EmailOptions = {
    from: mail.getSender(email.from),
    to: email.to,
    cc: email.cc && email.cc,
    bcc: email.bcc && email.bcc,
    replyTo: mail.getSender(email.replyTo),
    subject: email.subject,
    html: email.html,
    watchHtml: email.watchHtml && email.watchHtml,
  };

  logger.info('email options', options);

  const result = await new SES()
    .sendRawEmail({
      RawMessage: { Data: await EmailService.getRawEmail(options) },
    })
    .promise();

  logger.info('Sent email successfully', result);

  return result;
}
